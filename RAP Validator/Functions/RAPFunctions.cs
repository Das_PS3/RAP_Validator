﻿using Extra.Utilities;
using PKG_Extractor;
using RAP_Validator;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Threading;

namespace Extra.Functions
{
    internal class RAPFunctions
    {
        #region RAP-RIFKey conversion

        private static readonly byte[] e1 = { 0xA9, 0x3E, 0x1F, 0xD6, 0x7C, 0x55, 0xA3, 0x29, 0xB7, 0x5F, 0xDD, 0xA6, 0x2A, 0x95, 0xC7, 0xA5 };
        private static readonly byte[] e2 = { 0x67, 0xD4, 0x5D, 0xA3, 0x29, 0x6D, 0x00, 0x6A, 0x4E, 0x7C, 0x53, 0x7B, 0xF5, 0x53, 0x8C, 0x74 };
        private static readonly byte[] pbox = { 0x0C, 0x03, 0x06, 0x04, 0x01, 0x0B, 0x0F, 0x08, 0x02, 0x07, 0x00, 0x05, 0x0A, 0x0E, 0x0D, 0x09 };
        private static readonly byte[] rapInitialKey = { 0x86, 0x9F, 0x77, 0x45, 0xC1, 0x3F, 0xD8, 0x90, 0xCC, 0xF2, 0x91, 0x88, 0xE3, 0xCC, 0x3E, 0xDF };

        #endregion RAP-RIFKey conversion

        private const string RAPRegex = @"[A-Fa-f\d]{32}";

        internal static void GenerateRAP(Tuple<string, string> rap, string rapsDirectory)
        {
            if (!Directory.Exists(rapsDirectory))
                Directory.CreateDirectory(rapsDirectory);

            string rapFile = Path.Combine(rapsDirectory, rap.Item1 + ".rap");
            string rapValueStr = rap.Item2;

            byte[] rapValue = MiscUtils.HexStringToByteArray(rapValueStr);

            if (File.Exists(rapFile))
                File.SetAttributes(rapFile, FileAttributes.Normal);

            File.WriteAllBytes(rapFile, rapValue);
        }

        internal static bool? GetIsValidRAPFromFileHeader(byte[] fileToCheck, string rapsDirectory, string cid, string rap)
        {
            bool? result = null;

            if (File.Exists(rap))
            {
                string[] raps = File.ReadAllLines(rap);

                for (int i = 0; i < raps.Length; i++)
                {
                    if (!Regex.IsMatch(raps[i], RAPRegex) || raps[i].Length != 32)
                        continue;

                    if (fileToCheck.Length == 0x100)
                        result = EDAT.ValidateRAP(fileToCheck, RAPToRIFKey(MiscUtils.HexStringToByteArray(raps[i])));
                    else
                        result = EBOOT.ValidateRAP(fileToCheck, RAPToRIFKey(MiscUtils.HexStringToByteArray(raps[i])));

                    if (result.HasValue && result.Value)
                    {
                        File.WriteAllBytes(Path.Combine(rapsDirectory, cid + ".rap"), MiscUtils.HexStringToByteArray(raps[i]));

                        break;
                    }
                }
            }
            else
            {
                if (fileToCheck.Length == 0x100)
                    result = EDAT.ValidateRAP(fileToCheck, RAPToRIFKey(MiscUtils.HexStringToByteArray(rap)));
                else
                    result = EBOOT.ValidateRAP(fileToCheck, RAPToRIFKey(MiscUtils.HexStringToByteArray(rap)));
            }

            return result;
        }

        internal static bool? GetIsValidRAPFromLink(string link, string rap, string rapsDirectory)
        {
            byte[] headerCheckRaw = GetRawData(link, 0, 0x28);

            if (headerCheckRaw == null)
                return null;

            long checkHeaderEnd = GetCheckHeader(headerCheckRaw);

            byte[] rawCheckHeader = GetRawData(link, 0, checkHeaderEnd);

            var checkHeader = new PKGHeader(rawCheckHeader);

            if (!checkHeader.IsFinalizedValid || !checkHeader.IsMagicValid || !checkHeader.IsTypeValid)
                return null;

            long lastOffset = checkHeader.Entries.FirstOrDefault().FileContentsOffset;

            byte[] rawHeader = GetRawData(link, 0, checkHeader.EncryptedDataOffset + lastOffset);

            checkHeader = null;

            var header = new PKGHeader(rawHeader);

            PKGEntry smallestEntry;

            long startOffset;

            GetFileToCheck(header, out smallestEntry, out startOffset);

            if (smallestEntry == null)
                return null;

            if (link.EndsWith("_00.pkg"))
            {
                long pkgPart = 4687500000;

                for (int i = 0; i < 25; i++)
                    if (startOffset < pkgPart * (i + 1))
                    {
                        link = Regex.Replace(link, @"_\d\d\.pkg", string.Format("_{0:00}.pkg", i));

                        startOffset = startOffset - (pkgPart * i);

                        break;
                    }
            }

            byte[] rawFile = GetRawData(link, startOffset, startOffset + smallestEntry.FileContentsSize);

            while (rawFile.Length == 0)
            {
                Thread.Sleep(1000);

                rawFile = GetRawData(link, startOffset, startOffset + smallestEntry.FileContentsSize);
            }

            var extractor = new PKGExtractor();

            byte[] fileToCheck = extractor.DecryptRetailFile(header, smallestEntry, rawFile);

            return GetIsValidRAPFromFileHeader(fileToCheck, rapsDirectory, header.ContentID, rap);
        }

        internal static bool? GetIsValidRAPFromPKG(string file, string rap, string rapsDirectory)
        {
            if (!File.Exists(file))
                return null;

            using (var fs = new FileStream(file, FileMode.Open, FileAccess.Read))
            using (var br = new BinaryReader(fs))
            {
                byte[] headerRaw = br.ReadBytes(1024 * 1024 * 5);

                var header = new PKGHeader(headerRaw);

                if (!header.IsFinalizedValid || !header.IsMagicValid || !header.IsTypeValid)
                    return null;

                PKGEntry smallestEntry;

                long startOffset;

                GetFileToCheck(header, out smallestEntry, out startOffset);

                if (smallestEntry == null)
                    return null;

                fs.Position = startOffset;

                byte[] rawFile = br.ReadBytes((int)smallestEntry.FileContentsSize);

                var extractor = new PKGExtractor();

                byte[] fileToCheck = extractor.DecryptRetailFile(header, smallestEntry, rawFile);

                return GetIsValidRAPFromFileHeader(fileToCheck, rapsDirectory, header.ContentID, rap);
            }
        }

        private static long GetCheckHeader(byte[] headerCheckRaw)
        {
            using (var ms = new MemoryStream(headerCheckRaw))
            using (var br = new BinaryReader(ms))
            {
                ms.Position = 0x14;
                byte[] fileCountRaw = br.ReadBytes(4);

                if (BitConverter.IsLittleEndian)
                    Array.Reverse(fileCountRaw);

                long fileCount = BitConverter.ToInt32(fileCountRaw, 0);

                ms.Position = 0x20;
                byte[] encryptedDataOffsetRaw = br.ReadBytes(8);

                if (BitConverter.IsLittleEndian)
                    Array.Reverse(encryptedDataOffsetRaw);

                long encryptedDataOffset = BitConverter.ToInt64(encryptedDataOffsetRaw, 0);

                ms.Position = encryptedDataOffset;

                return encryptedDataOffset + 0x10;
            }
        }

        private static void GetFileToCheck(PKGHeader header, out PKGEntry smallestEntry, out long startOffset)
        {
            var exploitableFiles = new List<PKGEntry>();

            foreach (var headerEntry in header.Entries)
            {
                if (headerEntry.Filename == null)
                    continue;

                if (headerEntry.Filename.ToLower().EndsWith(".edat") || headerEntry.Filename.ToLower().EndsWith("eboot.bin"))
                    exploitableFiles.Add(headerEntry);
            }

            var smallestEntrySize = exploitableFiles.Any() ? exploitableFiles.Min(headerEntry => headerEntry.FileContentsSize) : 0;

            if (smallestEntrySize == 0)
            {
                smallestEntry = null;
                startOffset = 0;

                return;
            }

            smallestEntry = (from entry in exploitableFiles
                             where entry.FileContentsSize == smallestEntrySize
                             select entry).FirstOrDefault();

            startOffset = header.EncryptedDataOffset + smallestEntry.FileContentsOffset;

            long size;

            if (smallestEntry.Filename.ToLower().EndsWith(".edat"))
                size = 0x100;
            else
                size = 0x1000;

            byte[] sizeRaw = BitConverter.GetBytes(size);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(sizeRaw);

            smallestEntry.SetFileContentsSize(sizeRaw);
        }

        private static byte[] GetRawData(string link, long start, long end)
        {
            var getPKGHeader = (HttpWebRequest)WebRequest.Create(link);
            getPKGHeader.UserAgent = string.Empty;
            getPKGHeader.AddRange(start, end);

            try
            {
                using (var response = getPKGHeader.GetResponse())
                using (var data = response.GetResponseStream())
                using (var reader = new BinaryReader(data))
                {
                    byte[] rawPKGHeader = reader.ReadBytes((int)(end - start));

                    return rawPKGHeader;
                }
            }
            catch (WebException)
            {
                return null;
            }
        }

        private static byte[] RAPToRIFKey(byte[] rap)
        {
            byte[] rifKey = CryptographicEngines.Decrypt(rap, rapInitialKey, null, CipherMode.ECB, PaddingMode.None);

            for (int i = 0; i < 5; i++)
            {
                for (int n = 0; n < 16; n++)
                {
                    int p = pbox[n];

                    rifKey[p] ^= e1[p];
                }

                for (int n = 15; n >= 1; n--)
                {
                    int p = pbox[n];

                    int pp = pbox[n - 1];

                    rifKey[p] ^= rifKey[pp];
                }

                int x = 0;

                for (int n = 0; n < 16; n++)
                {
                    int p = pbox[n];

                    byte kc = (byte)(rifKey[p] - x);

                    byte ec2 = e2[p];

                    if (x != 1 || kc != 0xFF)
                    {
                        x = kc < ec2 ? 1 : 0;
                        rifKey[p] = (byte)(kc - ec2);
                    }
                    else if (kc == 0xFF)
                        rifKey[p] = (byte)(kc - ec2);
                    else
                        rifKey[p] = kc;
                }
            }

            return rifKey;
        }
    }
}
