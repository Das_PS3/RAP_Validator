﻿using Extra.Utilities;
using System;
using System.IO;
using System.Security.Cryptography;

namespace RAP_Validator
{
    internal class EDAT
    {
        private static readonly byte[][] _edatKeys = { new byte[] { 0xBE, 0x95, 0x9C, 0xA8, 0x30, 0x8D, 0xEF, 0xA2, 0xE5, 0xE1, 0x80, 0xC6, 0x37, 0x12, 0xA9, 0xAE },
                                                      new byte[] { 0x4C, 0xA9, 0xC1, 0x4B, 0x01, 0xC9, 0x53, 0x09, 0x96, 0x9B, 0xEC, 0x68, 0xAA, 0x0B, 0xC0, 0x81 } };

        internal static bool ValidateRAP(byte[] edat, byte[] rifKey)
        {
            using (var ms = new MemoryStream(edat))
            using (var br = new BinaryReader(ms))
            {
                ms.Position = 4;
                byte[] versionRaw = br.ReadBytes(4);

                if (BitConverter.IsLittleEndian)
                    Array.Reverse(versionRaw);

                int version = BitConverter.ToInt32(versionRaw, 0);

                ms.Position = 0x80;
                byte[] flagsRaw = br.ReadBytes(4);

                if (BitConverter.IsLittleEndian)
                    Array.Reverse(flagsRaw);

                int flags = BitConverter.ToInt32(flagsRaw, 0);

                ms.Position = 0;
                byte[] buffer = br.ReadBytes(0xA0);

                byte[] buffer2 = br.ReadBytes(0x10);

                int num = CheckHeader(rifKey, flags, version, buffer, buffer2);

                if ((num & 0x4000) == 0)
                    // RAP is valid
                    return true;
                else
                    // RAP is bad
                    return false;
            }
        }

        private static int CheckHeader(byte[] rifKey, int flags, int version, byte[] buffer, byte[] expectedRifKey)
        {
            byte[] @out = new byte[0xA0];

            if (version == 0 || version == 1)
            {
                if ((flags & 0x7ffffffe) != 0)
                    return 0x15;
            }
            else if (version == 2)
            {
                if ((flags & 0x7effffe0) != 0)
                    return 0x15;
            }
            else if (version <= 4)
            {
                if ((flags & 0x7effffc0) != 0)
                    return 0x15;
            }
            else
                return 0x16;

            if (((flags & 0x20) != 0) && ((flags & 1) != 0))
                return 0x15;

            int hashFlag = ((flags & 8) == 0) ? 2 : 0x10000002;

            if ((((ulong)flags) & 0x80000000) != 0)
                hashFlag |= 0x1000000;

            int keyIndex = version == 4 ? 1 : 0;

            var cmac = new CMAC();

            byte[] calculatedHash = new byte[rifKey.Length];

            GetHashKeys(hashFlag, ref calculatedHash, rifKey, keyIndex);

            cmac.DoInit(calculatedHash);

            cmac.DoUpdate(buffer, 0, buffer.Length);

            bool result = cmac.DoFinal(expectedRifKey, 0);

            if (!result)
            {
                if ((flags & 0x1000000) == 0x1000000)
                    return 0x13;

                return 0x4000;
            }

            return 0;
        }

        private static void GetHashKeys(int hashFlag, ref byte[] calculatedHash, byte[] hash, int keyIndex)
        {
            switch (((uint)(hashFlag & -268435456)))
            {
                case 0:
                    Array.Copy(hash, calculatedHash, calculatedHash.Length);

                    return;

                case 0x10000000:
                    calculatedHash = CryptographicEngines.Decrypt(hash, _edatKeys[keyIndex], new byte[0x10], CipherMode.CBC, PaddingMode.None);

                    return;

                case 0x20000000:
                    Array.Copy(_edatKeys[keyIndex], calculatedHash, calculatedHash.Length);

                    return;
            }

            throw new Exception("Hash mode is not valid: Undefined keys calculator");
        }
    }
}
