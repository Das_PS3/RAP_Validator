﻿using Extra.Utilities;
using System;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Security.Cryptography;

namespace PKG_Extractor
{
    internal class PKGExtractor
    {
        #region Internal methods

        /// <summary>
        /// Extracts a PS3 PKG file's contents.
        /// </summary>
        /// <param name="encryptedPkg">The encrypted PS3 PKG file.</param>
        /// <param name="outFolder">If defined, sets the folder to extract contents to.</param>
        /// <param name="requestedFiles">If defined, all files will be skipped except for this one.</param>
        internal void ExtractFiles(string encryptedPkg, string outFolder, string[] requestedFiles = null)
        {
            PKGHeader header = new PKGHeader(encryptedPkg);

            using (FileStream pkgFileStream = new FileStream(encryptedPkg, FileMode.Open, FileAccess.Read, FileShare.Read, 4096, FileOptions.RandomAccess))
            using (BinaryReader pkgFileReader = new BinaryReader(pkgFileStream))
            {
                for (int i = 0; i < header.Entries.Length; i++)
                {
                    var entry = header.Entries[i];

                    if (entry.IsDirectory)
                    {
                        Directory.CreateDirectory(Path.Combine(outFolder, entry.Filename));
                        continue;
                    }

                    if (requestedFiles != null && !requestedFiles.Any(x => entry.Filename.ToLower().Contains(x.ToLower())))
                        continue;

                    pkgFileStream.Position = header.EncryptedDataOffset + entry.FileContentsOffset;

                    if (header.IsFinalized)
                        ExtractFilesRetail(outFolder, requestedFiles, header, pkgFileStream, pkgFileReader, entry);
                    else
                        ExtractFilesDebug(outFolder, header, pkgFileStream, pkgFileReader, entry);
                }

                if (requestedFiles != null)
                    MiscUtils.DeleteEmptyDirectories(outFolder);
            }
        }

        internal byte[] DecryptRetailFile(PKGHeader header, PKGEntry entry, byte[] encryptedFile)
        {
            using (var encryptedFileStream = new MemoryStream(encryptedFile))
            using (var encryptedFileReader = new BinaryReader(encryptedFileStream))
            using (var decryptedFileStream = new MemoryStream())
            using (var decryptedFileWriter = new BinaryWriter(decryptedFileStream))
            {
                byte[] pkgRIVKeyIncrement = new byte[header.RIVKey.Length];

                Buffer.BlockCopy(header.RIVKey, 0, pkgRIVKeyIncrement, 0, pkgRIVKeyIncrement.Length);

                Array.Reverse(pkgRIVKeyIncrement);

                var pkgRIVKeyIncrementRaw = new BigInteger(pkgRIVKeyIncrement);

                pkgRIVKeyIncrementRaw += (entry.FileContentsOffset / pkgRIVKeyIncrement.Length);

                pkgRIVKeyIncrement = pkgRIVKeyIncrementRaw.ToByteArray();

                Array.Reverse(pkgRIVKeyIncrement);

                if (pkgRIVKeyIncrement.Length != header.RIVKey.Length)
                {
                    byte[] tmpArr = new byte[header.RIVKey.Length];

                    Buffer.BlockCopy(pkgRIVKeyIncrement, 0, tmpArr, 1, pkgRIVKeyIncrement.Length);

                    if (pkgRIVKeyIncrementRaw.Sign == 1)
                        tmpArr[0] = 0x00;
                    else if (pkgRIVKeyIncrementRaw.Sign == -1)
                        tmpArr[0] = 0xFF;

                    pkgRIVKeyIncrement = tmpArr;
                }

                while (encryptedFileStream.Position < entry.FileContentsSize)
                {
                    byte[] encryptedData;

                    int leftToReadRaw = (int)(entry.FileContentsSize - encryptedFileStream.Position);
                    int leftToRead = leftToReadRaw;

                    if ((leftToReadRaw / 16d) % 1 != 0)
                        leftToRead = MiscUtils.RoundUp(leftToRead, 16);

                    byte[] pkgRIVKeyConsecutive;

                    if (encryptedFileStream.Position <= entry.FileContentsSize - (header.aesKey.Length * 1024))
                    {
                        pkgRIVKeyConsecutive = SetRIVKeyConsecutive(pkgRIVKeyIncrement, header.aesKey.Length * 1024);

                        encryptedData = encryptedFileReader.ReadBytes(header.aesKey.Length * 1024); // Read 16384 bytes
                    }
                    else
                    {
                        pkgRIVKeyConsecutive = SetRIVKeyConsecutive(pkgRIVKeyIncrement, leftToRead);

                        encryptedData = encryptedFileReader.ReadBytes(leftToRead); // Read whatever is left to read for this file
                    }

                    byte[] pkgXORKeyConsecutive = null;

                    if (header.aesKey == PKGHeader.PS3AesKey)
                        pkgXORKeyConsecutive = CryptographicEngines.Encrypt(pkgRIVKeyConsecutive, PKGHeader.PS3AesKey, PKGHeader.PS3AesKey, CipherMode.ECB, PaddingMode.None);
                    else if (header.aesKey == PKGHeader.PSPAesKey)
                    {
                        if (entry.ContentType == 0 || entry.ContentType == 1 || entry.ContentType == 4)
                            pkgXORKeyConsecutive = CryptographicEngines.Encrypt(pkgRIVKeyConsecutive, PKGHeader.PS3AesKey, PKGHeader.PS3AesKey, CipherMode.ECB, PaddingMode.None);
                        else if (entry.ContentType == 2)
                            pkgXORKeyConsecutive = CryptographicEngines.Encrypt(pkgRIVKeyConsecutive, PKGHeader.PSPAesKey, null, CipherMode.ECB, PaddingMode.None);
                    }

                    byte[] decryptedData = CryptographicEngines.XOR(encryptedData, pkgXORKeyConsecutive);

                    if (((leftToReadRaw / 16d) % 1 != 0) && (leftToReadRaw < decryptedData.Length))
                        decryptedFileWriter.Write(decryptedData, 0, leftToReadRaw);
                    else
                        decryptedFileWriter.Write(decryptedData);
                }

                return decryptedFileStream.ToArray();
            }
        }

        #endregion Internal methods

        #region Private methods

        /// <summary>
        /// Decrypts and extracts a specific PKG_Entry from the provided debug PKG file.
        /// </summary>
        /// <param name="outFolder">Folder to extract the PKG_Entry into.</param>
        /// <param name="requestedFiles">If provided, the PKG_Entry will be decrypted and extracted only if the Filename property matches.</param>
        /// <param name="header">PKG_Header object containing the PKG_Entry.</param>
        /// <param name="pkgFileStream">Currently open FileStream to the PKG file.</param>
        /// <param name="pkgFileReader">Currently open BinaryReader to the PKG file.</param>
        /// <param name="pkgDebugKeyIncrement">Debug key.</param>
        /// <param name="entry">PKG_Entry object.</param>
        private void ExtractFilesDebug(string outFolder, PKGHeader header, FileStream pkgFileStream, BinaryReader pkgFileReader, PKGEntry entry)
        {
            byte[] pkgDebugKeyIncrement = new byte[header.DebugKey.Length];

            Buffer.BlockCopy(header.DebugKey, 0, pkgDebugKeyIncrement, 0, pkgDebugKeyIncrement.Length);

            byte[] baseKey = BitConverter.GetBytes(entry.FileContentsOffset / 0x10);
            Array.Reverse(baseKey);
            Buffer.BlockCopy(baseKey, 0, pkgDebugKeyIncrement, pkgDebugKeyIncrement.Length - baseKey.Length, baseKey.Length);

            using (FileStream outFileStream = new FileStream(Path.Combine(outFolder, entry.Filename), FileMode.Create, FileAccess.Write, FileShare.Read, 4096, FileOptions.SequentialScan))
            using (BinaryWriter outFileWriter = new BinaryWriter(outFileStream))
                while (pkgFileStream.Position < (header.EncryptedDataOffset + entry.FileContentsOffset + entry.FileContentsSize))
                {
                    byte[] encryptedData;

                    int leftToReadRaw = (int)(header.EncryptedDataOffset + entry.FileContentsOffset + entry.FileContentsSize - pkgFileStream.Position);
                    int leftToRead = leftToReadRaw;

                    if ((leftToReadRaw / 16d) % 1 != 0)
                        leftToRead = MiscUtils.RoundUp(leftToRead, 16);

                    byte[] pkgXorKeyConsecutive;

                    if (pkgFileStream.Position <= (header.EncryptedDataOffset + entry.FileContentsOffset + entry.FileContentsSize) - (header.aesKey.Length * 1024))
                    {
                        pkgXorKeyConsecutive = SetDebugXorKeyConsecutive(ref pkgDebugKeyIncrement, header.aesKey.Length * 1024);

                        encryptedData = pkgFileReader.ReadBytes(header.aesKey.Length * 1024); // Read 16384 bytes
                    }
                    else
                    {
                        pkgXorKeyConsecutive = SetDebugXorKeyConsecutive(ref pkgDebugKeyIncrement, leftToRead);

                        encryptedData = pkgFileReader.ReadBytes(leftToRead); // Read whatever is left to read for this file
                    }

                    byte[] decryptedData = CryptographicEngines.XOR(encryptedData, pkgXorKeyConsecutive);

                    if (((leftToReadRaw / 16d) % 1 != 0) && (leftToReadRaw < decryptedData.Length))
                        outFileWriter.Write(decryptedData, 0, leftToReadRaw);
                    else
                        outFileWriter.Write(decryptedData);
                }
        }

        /// <summary>
        /// Decrypts and extracts a specific PKG_Entry from the provided retail PKG file.
        /// </summary>
        /// <param name="outFolder">Folder to extract the PKG_Entry into.</param>
        /// <param name="requestedFiles">If provided, the PKG_Entry will be decrypted and extracted only if the Filename property matches.</param>
        /// <param name="header">PKG_Header object containing the PKG_Entry.</param>
        /// <param name="pkgFileStream">Currently open FileStream to the PKG file.</param>
        /// <param name="pkgFileReader">Currently open BinaryReader to the PKG file.</param>
        /// <param name="pkgRivKeyIncrement">RIV key.</param>
        /// <param name="entry">PKG_Entry object.</param>
        private void ExtractFilesRetail(string outFolder, string[] requestedFiles, PKGHeader header, FileStream pkgFileStream, BinaryReader pkgFileReader, PKGEntry entry)
        {
            byte[] pkgRIVKeyIncrement = new byte[header.RIVKey.Length];

            Buffer.BlockCopy(header.RIVKey, 0, pkgRIVKeyIncrement, 0, pkgRIVKeyIncrement.Length);

            Array.Reverse(pkgRIVKeyIncrement);

            var pkgRIVKeyIncrementRaw = new BigInteger(pkgRIVKeyIncrement);

            pkgRIVKeyIncrementRaw += (entry.FileContentsOffset / pkgRIVKeyIncrement.Length);

            pkgRIVKeyIncrement = pkgRIVKeyIncrementRaw.ToByteArray();

            Array.Reverse(pkgRIVKeyIncrement);

            if (pkgRIVKeyIncrement.Length != header.RIVKey.Length)
            {
                byte[] tmpArr = new byte[header.RIVKey.Length];

                Buffer.BlockCopy(pkgRIVKeyIncrement, 0, tmpArr, 1, pkgRIVKeyIncrement.Length);

                if (pkgRIVKeyIncrementRaw.Sign == 1)
                    tmpArr[0] = 0x00;
                else if (pkgRIVKeyIncrementRaw.Sign == -1)
                    tmpArr[0] = 0xFF;

                pkgRIVKeyIncrement = tmpArr;
            }

            using (FileStream outFileStream = new FileStream(Path.Combine(outFolder, entry.Filename), FileMode.Create, FileAccess.Write, FileShare.Read, 4096, FileOptions.SequentialScan))
            using (BinaryWriter outFileWriter = new BinaryWriter(outFileStream))
                while (pkgFileStream.Position < (header.EncryptedDataOffset + entry.FileContentsOffset + entry.FileContentsSize))
                {
                    byte[] encryptedData;

                    int leftToReadRaw = (int)(header.EncryptedDataOffset + entry.FileContentsOffset + entry.FileContentsSize - pkgFileStream.Position);
                    int leftToRead = leftToReadRaw;

                    if ((leftToReadRaw / 16d) % 1 != 0)
                        leftToRead = MiscUtils.RoundUp(leftToRead, 16);

                    byte[] pkgRIVKeyConsecutive;

                    if (pkgFileStream.Position <= (header.EncryptedDataOffset + entry.FileContentsOffset + entry.FileContentsSize) - (header.aesKey.Length * 1024))
                    {
                        pkgRIVKeyConsecutive = SetRIVKeyConsecutive(pkgRIVKeyIncrement, header.aesKey.Length * 1024);

                        encryptedData = pkgFileReader.ReadBytes(header.aesKey.Length * 1024); // Read 16384 bytes
                    }
                    else
                    {
                        pkgRIVKeyConsecutive = SetRIVKeyConsecutive(pkgRIVKeyIncrement, leftToRead);

                        encryptedData = pkgFileReader.ReadBytes(leftToRead); // Read whatever is left to read for this file
                    }

                    byte[] pkgXORKeyConsecutive = null;

                    if (header.aesKey == PKGHeader.PS3AesKey)
                        pkgXORKeyConsecutive = CryptographicEngines.Encrypt(pkgRIVKeyConsecutive, PKGHeader.PS3AesKey, PKGHeader.PS3AesKey, CipherMode.ECB, PaddingMode.None);
                    else if (header.aesKey == PKGHeader.PSPAesKey)
                    {
                        if (entry.ContentType == 0 || entry.ContentType == 1 || entry.ContentType == 4)
                            pkgXORKeyConsecutive = CryptographicEngines.Encrypt(pkgRIVKeyConsecutive, PKGHeader.PS3AesKey, PKGHeader.PS3AesKey, CipherMode.ECB, PaddingMode.None);
                        else if (entry.ContentType == 2)
                            pkgXORKeyConsecutive = CryptographicEngines.Encrypt(pkgRIVKeyConsecutive, PKGHeader.PSPAesKey, null, CipherMode.ECB, PaddingMode.None);
                    }

                    byte[] decryptedData = CryptographicEngines.XOR(encryptedData, pkgXORKeyConsecutive);

                    if (((leftToReadRaw / 16d) % 1 != 0) && (leftToReadRaw < decryptedData.Length))
                        outFileWriter.Write(decryptedData, 0, leftToReadRaw);
                    else
                        outFileWriter.Write(decryptedData);
                }
        }

        /// <summary>
        /// Set the pkgRivKeyConsecutive byte array based on the amount of bytes to be read.
        /// </summary>
        /// <param name="pkgRIVKeyIncrement">PKG RIV Key Increment</param>
        /// <param name="toRead">Amount of bytes that will be read.</param>
        /// <returns></returns>
        private byte[] SetRIVKeyConsecutive(byte[] pkgRIVKeyIncrement, int toRead)
        {
            byte[] pkgRIVKeyConsecutive = new byte[toRead];

            for (int position = 0; position < toRead; position += pkgRIVKeyIncrement.Length)
            {
                Buffer.BlockCopy(pkgRIVKeyIncrement, 0, pkgRIVKeyConsecutive, position, pkgRIVKeyIncrement.Length);

                CryptographicEngines.IncrementArrayAtIndex(ref pkgRIVKeyIncrement, pkgRIVKeyIncrement.Length - 1);
            }

            return pkgRIVKeyConsecutive;
        }

        /// <summary>
        /// Set the debug pkgXorKeyConsecutive byte array based on the amount of bytes to be read.
        /// </summary>
        /// <param name="pkgDebugKeyIncrement">PKG debug Key Increment</param>
        /// <param name="toRead">Amount of bytes that will be read.</param>
        /// <returns></returns>
        private byte[] SetDebugXorKeyConsecutive(ref byte[] pkgDebugKeyIncrement, int toRead)
        {
            byte[] pkgXorKeyConsecutive = new byte[toRead];

            for (int position = 0; position < toRead; position += 0x10)
            {
                using (SHA1Managed hasher = new SHA1Managed())
                {
                    byte[] keyHash = hasher.ComputeHash(pkgDebugKeyIncrement);
                    Buffer.BlockCopy(keyHash, 0, pkgXorKeyConsecutive, position, 0x10);
                }

                CryptographicEngines.IncrementArrayAtIndex(ref pkgDebugKeyIncrement, pkgDebugKeyIncrement.Length - 1);
            }

            return pkgXorKeyConsecutive;
        }

        #endregion Private methods
    }
}
