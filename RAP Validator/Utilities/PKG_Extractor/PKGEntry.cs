﻿using Extra.Utilities;
using System;
using System.IO;
using System.Text;

namespace PKG_Extractor
{
    internal class PKGEntry
    {
        #region Fields

        private byte contentType;
        private long fileContentsOffset, fileContentsSize;
        private string filename;
        private int filenameOffset, filenameSize;
        private bool isDirectory;

        #endregion Fields

        #region Constructor

        internal PKGEntry(byte[] rawPKGEntry)
        {
            using (var readStream = new MemoryStream(rawPKGEntry))
            using (var reader = new BinaryReader(readStream))
            {
                if (readStream.Position >= readStream.Length)
                    return;

                SetFilenameOffset(reader.ReadBytes(4));

                if (readStream.Position >= readStream.Length)
                    return;

                SetFilenameSize(reader.ReadBytes(4));

                if (readStream.Position >= readStream.Length)
                    return;

                SetFileContentsOffset(reader.ReadBytes(8));

                if (readStream.Position >= readStream.Length)
                    return;

                SetFileContentsSize(reader.ReadBytes(8));

                if (readStream.Position >= readStream.Length)
                    return;

                readStream.Position += 2; // Padding

                SetContentType(reader.ReadByte());

                SetIsDirectory(reader.ReadByte());
            }
        }

        #endregion Constructor

        #region Properties

        internal byte ContentType { get { return contentType; } }
        internal long FileContentsOffset { get { return fileContentsOffset; } }
        internal long FileContentsSize { get { return fileContentsSize; } }
        internal string Filename { get { return filename; } }
        internal int FilenameOffset { get { return filenameOffset; } }
        internal int FilenameSize { get { return filenameSize; } }
        internal bool IsDirectory { get { return isDirectory; } }

        #endregion Properties

        #region Internal Methods

        internal void SetFileContentsSize(byte[] value)
        {
            MiscUtils.CheckEndianness(ref value);

            fileContentsSize = BitConverter.ToInt64(value, 0);
        }

        internal void SetFilename(byte[] value)
        {
            filename = Encoding.UTF8.GetString(value);

            if (value.Length > filenameSize)
                filename = filename.Remove(filenameSize);
        }

        #endregion Internal Methods

        #region Private Methods

        private void SetContentType(byte value)
        {
            contentType = value;
        }

        private void SetFileContentsOffset(byte[] value)
        {
            MiscUtils.CheckEndianness(ref value);

            fileContentsOffset = BitConverter.ToInt64(value, 0);
        }

        private void SetFilenameOffset(byte[] value)
        {
            MiscUtils.CheckEndianness(ref value);

            filenameOffset = BitConverter.ToInt32(value, 0);
        }

        private void SetFilenameSize(byte[] value)
        {
            MiscUtils.CheckEndianness(ref value);

            filenameSize = BitConverter.ToInt32(value, 0);
        }

        private void SetIsDirectory(byte value)
        {
            isDirectory = value == 0x04 ? true : false;
        }

        #endregion Private Methods
    }
}
