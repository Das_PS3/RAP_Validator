﻿using Extra.Functions;
using System;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace RAP_Validator
{
    internal class Program
    {
        private const string LinkRegex = @"http://.*?\.pkg";
        private const string RAPRegex = @"[A-Fa-f\d]{32}";
        private const string RAPsDirectory = "raps";
        private const string SplitNonFirstRegex = @"_\d[1-9]\.pkg";

        private static bool? checkEBOOTFile(string file, string rap)
        {
            if (CheckLicenseTypeIsFree(file))
                return null;

            byte[] fileHeader;

            string cid;

            using (var fs = new FileStream(file, FileMode.Open, FileAccess.Read))
            using (var br = new BinaryReader(fs))
            {
                fileHeader = br.ReadBytes(0x1000);

                fs.Position = 0xE;

                byte[] offset = br.ReadBytes(2);

                if (BitConverter.IsLittleEndian)
                    Array.Reverse(offset);

                fs.Position = BitConverter.ToInt16(offset, 0) - 0x50;

                cid = Encoding.UTF8.GetString(br.ReadBytes(0x24));
            }

            return RAPFunctions.GetIsValidRAPFromFileHeader(fileHeader, RAPsDirectory, cid, rap);
        }

        private static bool? CheckEDATFile(string file, string rap)
        {
            if (CheckLicenseTypeIsFree(file))
                return null;

            byte[] fileHeader;

            using (var fs = new FileStream(file, FileMode.Open, FileAccess.Read))
            using (var br = new BinaryReader(fs))
                fileHeader = br.ReadBytes(0x100);

            string cid = Encoding.UTF8.GetString(fileHeader, 0x10, 0x24);

            return RAPFunctions.GetIsValidRAPFromFileHeader(fileHeader, RAPsDirectory, cid, rap);
        }

        private static bool CheckIsNPDRM(string file)
        {
            using (var fs = File.Open(file, FileMode.Open, FileAccess.Read))
            using (var br = new BinaryReader(fs))
            {
                fs.Position = 0x7F;

                if (br.ReadByte() != 8)
                {
                    Console.WriteLine(string.Format("[{0}] {1}", DateTime.Now.TimeOfDay.ToString(@"hh\:mm\:ss"), string.Format("The file \"{0}\" is not a supported NPDRM file.", file)));

                    return false;
                }

                return true;
            }
        }

        private static bool CheckLicenseTypeIsFree(string file)
        {
            byte licenseCheckRaw = GetLicenseTypeFromFile(file);

            if (licenseCheckRaw == 3)
            {
                Console.WriteLine(string.Format("[{0}] {1}", DateTime.Now.TimeOfDay.ToString(@"hh\:mm\:ss"), string.Format("The license type for the file \"{0}\" is free and does not require any RAP value.", file)));

                return true;
            }

            return false;
        }

        private static bool CheckParameters(string[] args)
        {
            if (args.Length != 2)
            {
                Console.WriteLine("Missing parameters." + Environment.NewLine);

                Console.WriteLine("Usage: " + Environment.NewLine);

                Console.WriteLine('"' + Assembly.GetExecutingAssembly().GetName().Name + "\" link/pkg_file/links_text_file rap_value/raps_text_file/rap_file" + Environment.NewLine);

                Console.WriteLine("rap_value must be provided as a hexadecimal string of 32 without spaces.");
                Console.WriteLine("Example: \"" + Assembly.GetExecutingAssembly().GetName().Name +
                                  "\" http://zeus.dl.playstation.net/cdn/EP4492/NPEB02382_00/EP4492-NPEB02382_00-COLOR00000000000_bg_1_a3b7a3613d281fd89424e003e4341c6172292274.pkg" +
                                  " 7FE043FBE545FB84B52E0C5080469E8C");

                return false;
            }

            if ((!Regex.IsMatch(args[0], LinkRegex, RegexOptions.RightToLeft) && !File.Exists(args[0])) || (!Regex.IsMatch(args[1], RAPRegex) && args[1].Length != 0x20 && !File.Exists(args[1])))
            {
                Console.WriteLine("ERROR: Invalid file/link or RAP value/file.");

                return false;
            }

            return true;
        }

        private static bool? CheckPKGFile(string file, string rap)
        {
            if (CheckLicenseTypeIsFree(file))
                return null;

            return RAPFunctions.GetIsValidRAPFromPKG(file, rap, RAPsDirectory);
        }

        private static bool? CheckPKGLink(string[] args)
        {
            if (Regex.IsMatch(args[0], SplitNonFirstRegex))
                args[0] = Regex.Replace(args[0], SplitNonFirstRegex, @"_00.pkg");
            byte licenseCheckRaw = GetLicenseTypeFromLink(args[0]);

            if (licenseCheckRaw == 3)
            {
                Console.WriteLine(string.Format("[{0}] {1}", DateTime.Now.TimeOfDay.ToString(@"hh\:mm\:ss"), string.Format("The license type for the link {0} is free and does not require any RAP value.", args[0])));

                return null;
            }

            return RAPFunctions.GetIsValidRAPFromLink(args[0], args[1].ToUpper(), RAPsDirectory);
        }

        private static void CheckTXTFile(string[] args)
        {
            string[] links = File.ReadAllLines(args[0]);

            for (int i = 0; i < links.Length; i++)
            {
                if (Regex.IsMatch(links[i], LinkRegex) || File.Exists(links[i]))
                {
                    if (CheckLicenseTypeIsFree(args[0]))
                        continue;

                    bool? isValidRAP = null;

                    if (File.Exists(links[i]) && links[i].EndsWith(".pkg", StringComparison.InvariantCultureIgnoreCase))
                        isValidRAP = CheckPKGFile(links[i], args[1]);
                    else if (File.Exists(links[i]) && links[i].EndsWith(".edat", StringComparison.InvariantCultureIgnoreCase))
                        isValidRAP = CheckEDATFile(links[i], args[1]);
                    else if (File.Exists(links[i]) && links[i].EndsWith("eboot.bin", StringComparison.InvariantCultureIgnoreCase))
                        isValidRAP = checkEBOOTFile(links[i], args[1]);
                    else if (!File.Exists(links[i]))
                        isValidRAP = RAPFunctions.GetIsValidRAPFromLink(links[i], args[1], RAPsDirectory);

                    DisplayResults(isValidRAP, links[i], args[1]);
                }
            }
        }

        private static void DisplayResults(bool? isValidRAP, string pkg, string rap)
        {
            if (!File.Exists(rap))
            {
                rap = rap.ToUpper();

                if (isValidRAP.HasValue && isValidRAP.Value)
                    Console.WriteLine(string.Format("[{0}] {1}", DateTime.Now.TimeOfDay.ToString(@"hh\:mm\:ss"), string.Format("The RAP value {0} for {1} has been validated successfully.", rap, pkg)));
                else if (isValidRAP.HasValue && !isValidRAP.Value)
                    Console.WriteLine(string.Format("[{0}] {1}", DateTime.Now.TimeOfDay.ToString(@"hh\:mm\:ss"), string.Format("The RAP value {0} is not valid for {1}.", rap, pkg)));
                else if (!isValidRAP.HasValue)
                    Console.WriteLine(string.Format("[{0}] {1}", DateTime.Now.TimeOfDay.ToString(@"hh\:mm\:ss"), string.Format("Warning: The RAP value {0} attached to {1} could not be validated.", rap.ToUpper(), pkg)));
            }
            else
            {
                if (isValidRAP.HasValue && isValidRAP.Value)
                    Console.WriteLine(string.Format("[{0}] {1}", DateTime.Now.TimeOfDay.ToString(@"hh\:mm\:ss"), string.Format("The RAPs file {0} for {1} has been validated successfully and the proper RAP file has been saved.", rap, pkg)));
                else if (isValidRAP.HasValue && !isValidRAP.Value)
                    Console.WriteLine(string.Format("[{0}] {1}", DateTime.Now.TimeOfDay.ToString(@"hh\:mm\:ss"), string.Format("The RAPs file {0} does not contain a valid RAP value for {1}.", rap, pkg)));
                else if (!isValidRAP.HasValue)
                    Console.WriteLine(string.Format("[{0}] {1}", DateTime.Now.TimeOfDay.ToString(@"hh\:mm\:ss"), string.Format("Warning: The RAPs file {0} attached to {1} could not be validated.", rap, pkg)));
            }
        }

        private static byte GetLicenseTypeFromFile(string file)
        {
            using (var fs = File.Open(file, FileMode.Open, FileAccess.Read))
            using (var br = new BinaryReader(fs))
            {
                if (file.EndsWith(".pkg", StringComparison.InvariantCultureIgnoreCase))
                    fs.Position = 0xCB;
                else if (file.EndsWith(".edat", StringComparison.InvariantCultureIgnoreCase))
                    fs.Position = 0xB;
                else if (file.EndsWith("eboot.bin", StringComparison.InvariantCultureIgnoreCase))
                {
                    fs.Position = 0xE;

                    byte[] offset = br.ReadBytes(2);

                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(offset);

                    fs.Position = BitConverter.ToInt16(offset, 0) - 0x55;
                }

                byte rawLicenseType = br.ReadByte();

                return rawLicenseType;
            }
        }

        private static byte GetLicenseTypeFromLink(string link)
        {
            var getRawLicenseType = (HttpWebRequest)WebRequest.Create(link);
            getRawLicenseType.UserAgent = string.Empty;
            getRawLicenseType.AddRange(0xCB, 0xCB);

            try
            {
                using (var response = getRawLicenseType.GetResponse())
                using (var data = response.GetResponseStream())
                using (var reader = new BinaryReader(data))
                {
                    byte rawLicenseType = reader.ReadByte();

                    return rawLicenseType;
                }
            }
            catch (WebException)
            {
                return 0;
            }
        }

        private static void Main(string[] args)
        {
            if (!CheckParameters(args))
            {
#if DEBUG
                Console.ReadKey(true);
#endif

                return;
            }

            Console.WriteLine(string.Format("[{0}] Now working, please wait...", DateTime.Now.TimeOfDay.ToString(@"hh\:mm\:ss")));

            if (!Directory.Exists(RAPsDirectory))
                Directory.CreateDirectory(RAPsDirectory);

            if (File.Exists(args[1]) && args[1].EndsWith(".rap", StringComparison.InvariantCultureIgnoreCase))
                args[1] = BitConverter.ToString(File.ReadAllBytes(args[1])).Replace("-", "");

            bool? isValidRAP = null;

            if (!File.Exists(args[0]))
                isValidRAP = CheckPKGLink(args);

            if (File.Exists(args[0]) && args[0].EndsWith(".pkg", StringComparison.InvariantCultureIgnoreCase))
                isValidRAP = CheckPKGFile(args[0], args[1]);

            if (File.Exists(args[0]) && args[0].EndsWith(".txt", StringComparison.InvariantCultureIgnoreCase))
            {
                CheckTXTFile(args);

                Console.WriteLine(string.Format("[{0}] All tasks finished.", DateTime.Now.TimeOfDay.ToString(@"hh\:mm\:ss")));

#if DEBUG
                Console.ReadKey(true);
#endif

                return;
            }

            if (File.Exists(args[0]) && args[0].EndsWith(".edat", StringComparison.InvariantCultureIgnoreCase))
                isValidRAP = CheckEDATFile(args[0], args[1]);

            if (File.Exists(args[0]) && args[0].EndsWith("eboot.bin", StringComparison.InvariantCultureIgnoreCase))
            {
                if (!CheckIsNPDRM(args[0]))
                {
#if DEBUG
                    Console.ReadKey(true);
#endif

                    return;
                }

                isValidRAP = checkEBOOTFile(args[0], args[1]);
            }

            DisplayResults(isValidRAP, args[0], args[1]);

            Console.WriteLine(string.Format("[{0}] All tasks finished.", DateTime.Now.TimeOfDay.ToString(@"hh\:mm\:ss")));

#if DEBUG
            Console.ReadKey(true);
#endif
        }
    }
}
